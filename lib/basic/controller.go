package basic

import (
	"context"
	"log"

	"gitlab.com/seagulls/event/lib/events"
)

type Controller struct {
	ex events.Exchange
	q  events.Queue
	f  events.WorkerFactory
}

func NewController(ex events.Exchange, q events.Queue, f events.WorkerFactory) *Controller {
	return &Controller{
		ex: ex,
		q:  q,
		f:  f,
	}
}

func (c *Controller) Start(ctx context.Context) {
	log.Printf("Controller: starting...")

	for i := 0; i < 2; i++ {
		wrk := c.f.New(ctx, c)
		go wrk.Start(ctx)
	}
	go c.ex.Start(ctx)
}

func (c *Controller) Init(context.Context) error {
	return nil
}

func (c *Controller) Shutdown(context.Context) error {
	return nil
}

func (c *Controller) Get(ctx context.Context) (events.Message, error) {
	return c.q.Get(ctx)
}

func (c *Controller) Ack(ctx context.Context, msg events.Message) error {
	return c.q.Ack(ctx, msg)
}

// Put places event messages onto the external exchange. Any non-event
// messages are put on the internal queue.
func (c *Controller) Put(ctx context.Context, msg events.Message) error {
	if msg.Type() == events.TypeEvent {
		return c.ex.Put(ctx, msg)
	}

	return c.q.Put(ctx, msg)
}
