package basic

import (
	"gitlab.com/seagulls/event/lib/events"
)

// Queue is a basic implementation of the events.Queue, as the
// source and sink are anonymous we do not have to implement the
// interface methods making this a very simple implementation.
type Queue struct {
	events.Source
	events.Sink
}
