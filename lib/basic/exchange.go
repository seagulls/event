package basic

import (
	"context"
	"log"

	"gitlab.com/seagulls/event/lib/events"
)

// Exchange is a minimal implementation of an exchange adapter.
// There is no failure on messages failing to push nor ack on
// the assumption that those will simply be republished as event
// consequences are idempotent.
type Exchange struct {
	reg      *events.MessageRegistry
	exSource events.Source
	exSink   events.Sink
	inSink   events.Sink
}

// NewExchange does exactly what you think it does...
func NewExchange(reg *events.MessageRegistry, exSource events.Source, exSink, inSink events.Sink) *Exchange {
	return &Exchange{
		reg:      reg,
		exSource: exSource,
		exSink:   exSink,
		inSink:   inSink,
	}
}

// Start fires up a single exchange worker which will shovel
// events from the exchange source to the internal queue sink.
func (e *Exchange) Start(ctx context.Context) {
	log.Printf("Exchange: starting...")
	go eWorker(ctx, e.reg, e.exSource, e.inSink)
}

func (e *Exchange) Init(context.Context) error { return nil }

func (e *Exchange) Shutdown(context.Context) error { return nil }

// Put places event messages onto the external exchange. Any non-event
// messages are silently dropped.
func (e *Exchange) Put(ctx context.Context, msg events.Message) error {
	if msg.Type() != events.TypeEvent {
		return nil
	}

	return e.exSink.Put(ctx, msg)
}

func eWorker(ctx context.Context, reg *events.MessageRegistry, in events.Source, out events.Sink) {
	log.Printf("ExchangeWorker: starting...")
	for {
		msg, err := in.Get(ctx)
		if err != nil {
			// log.Printf("ExchangeWorker: error getting message - %s", err.Error())
			return
		}

		// if the event name is not registered, drop it
		if _, ok := reg.Get(msg.Name()); !ok {
			// log.Printf("ExchangeWorker: dropping message %q, not interested", msg.Name())
			continue
		}

		err = out.Put(ctx, msg)
		if err != nil {
			continue
		}

		_ = in.Ack(ctx, msg)
	}
}
