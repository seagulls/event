package mem

import (
	"context"
	"errors"

	"gitlab.com/seagulls/event/lib/events"
)

type Queue struct {
	data chan events.Message
}

func NewQueue() *Queue {
	return &Queue{
		data: make(chan events.Message, 100),
	}
}

func (q *Queue) Get(ctx context.Context) (events.Message, error) {
	select {
	case <-ctx.Done():
		return nil, errors.New("timeout")
	case msg, ok := <-q.data:
		if !ok {
			return nil, errors.New("queue closed")
		}
		return msg, nil
	}
}

func (q *Queue) Ack(ctx context.Context, msg events.Message) error {
	return nil
}

func (q *Queue) Put(ctx context.Context, msg events.Message) error {
	select {
	case <-ctx.Done():
		return errors.New("timeout")
	case q.data <- msg:
		return nil
	}
}
