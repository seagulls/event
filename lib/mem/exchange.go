package mem

import (
	"context"
	"errors"
	"log"

	"gitlab.com/seagulls/event/lib/events"
)

type Exchange struct {
	subs map[string]*subscriber
}

func NewExchange() *Exchange {
	return &Exchange{
		subs: make(map[string]*subscriber),
	}
}

func (e *Exchange) Register(name string) events.Queue {
	if s, ok := e.subs[name]; ok {
		return s
	}

	s := &subscriber{
		putFn: e.Put,
		data:  make(chan events.Message, 100),
	}
	e.subs[name] = s
	return s
}

func (e *Exchange) Put(ctx context.Context, msg events.Message) error {
	log.Printf("Exchange: %s", msg.String())

	for _, sub := range e.subs {
		select {
		case <-ctx.Done():
			return errors.New("timeout")
		case sub.data <- msg:
			// log.Printf("Exchange: distributed message to %s\n", k)
		}
	}

	return nil
}

type subscriber struct {
	putFn func(context.Context, events.Message) error
	data  chan events.Message
}

func (e *subscriber) Put(ctx context.Context, msg events.Message) error {
	return e.putFn(ctx, msg)
}

func (e *subscriber) Get(ctx context.Context) (events.Message, error) {
	select {
	case <-ctx.Done():
		return nil, errors.New("timeout")
	case msg, ok := <-e.data:
		if !ok {
			return nil, errors.New("queue closed")
		}
		return msg, nil
	}
}

func (e *subscriber) Ack(ctx context.Context, msg events.Message) error {
	return nil
}
