package events

import (
	"context"
	"errors"
	"reflect"
)

type MessageHandler func(context.Context, Message) error

type HandlerRegistry struct {
	reg map[reflect.Type]MessageHandler
}

func NewHandlerRegistry() *HandlerRegistry {
	return &HandlerRegistry{
		reg: make(map[reflect.Type]MessageHandler),
	}
}

func (r *HandlerRegistry) Register(m Message, hdl MessageHandler) error {
	rv := reflect.ValueOf(m)
	rt := rv.Type()

	if _, ok := r.reg[rt]; ok {
		return errors.New("already registered")
	}

	r.reg[rt] = hdl

	return nil
}

func (r *HandlerRegistry) Get(m Message) (MessageHandler, bool) {
	rt, ok := r.reg[reflect.TypeOf(m)]

	return rt, ok
}
