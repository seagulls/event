package events

import "context"

type MessageType string

const (
	TypeCommand MessageType = `command`
	TypeEvent   MessageType = `event`
)

// Message is the abstract form of a message, and may represent
// any type of message.
type Message interface {
	ID() string
	Name() string
	Type() MessageType
	String() string
}

// Command is a specific message type instructing a service to
// perform an action.
type Command interface {
	Message
}

// Event is a specific message detailing a change of state within
// a business domain.
type Event interface {
	Message
}

// Worker represents an asynchronous process which repeatedly carries
// out a specific task. The Worker will continue carrying out the task
// as long as the context passed in remains live. Should the context be
// cancelled the Worker is responsible for tidying any resources it is
// using.
type Worker interface {
	Start(context.Context)
}

// WorkerFactory produces workers for the exchange to allocate to
// processing messages from the internal queue.
type WorkerFactory interface {
	New(context.Context, Controller) Worker
}

// Service represents a persistent service which has resources which
// require some setup before starting and/or teardown on exit.
type Service interface {
	Worker
	Init(context.Context) error
	Shutdown(context.Context) error
}

// Source is the adapter which fetches messages and acknowledge that
// they have been dealt with. Does not matter where they are coming from.
type Source interface {
	// Get fetches a message from the Source. This method blocks until a
	// message is available or the context expires.
	Get(context.Context) (Message, error)
	// Ack a message once it has been processed.
	Ack(context.Context, Message) error
}

// Sink is an adapter for publishing messages. Distinction between different
// types of messages is obfuscated intentionally as the consumer does not
// need to know where it's messages are going.
type Sink interface {
	// Put a message to the Sink, this method may block if the Sink is
	// out of capacity and may fail if capacity is not available before
	// the context expires.
	Put(context.Context, Message) error
}

// Exchange adapters form the interface to the central event exchange.  Exchange
// implementations will typically have at least one internal Sink and an external
// Source and Sink. Exchanges deal exclusively with Event type messages and should
// ack messages from the external source on successfully passing them through to
// the internal Sink via internal Exchange workers.
type Exchange interface {
	Service
	Sink
}

// Queue is the adapter for the microservice's internal message queue, this will
// hold all message types.
type Queue interface {
	Source
	Sink
}

// Controller is the interface for the microservice domain controller and acts
// as the controller for the Exchange and the Queue. Domain workers are created
// by the Controller, directing Event messages to the Exchange.Sink and Command
// messages to the DomainQueue.Sink.
type Controller interface {
	Service
	Source
	Sink
}
