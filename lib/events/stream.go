package events

import (
	"encoding/json"
	"errors"
	"io"
	"reflect"
)

// An Encoder writes JSON values to an output stream. This is
// generically bound to event.Message types.
type Encoder[T Message] struct {
	json *json.Encoder
}

// NewEncoder returns a new encoder that writes to w.
func NewEncoder[T Message](w io.Writer) *Encoder[T] {
	return &Encoder[T]{
		json: json.NewEncoder(w),
	}
}

// Encode writes the JSON encoding of t to the stream,
// followed by a newline character.
//
// See the documentation for Marshal for details about the
// conversion of Go values to JSON.
func (enc *Encoder[T]) Encode(t T) error {
	return enc.json.Encode(t)
}

// A Decoder reads and decodes JSON values from an input stream. This
// is generically bound to event.Mesage types.
type Decoder[T Message] struct {
	json *json.Decoder
}

// NewDecoder returns a new decoder that reads from r.
//
// The decoder introduces its own buffering and may
// read data from r beyond the JSON values requested.
func NewDecoder[T Message](r io.Reader) *Decoder[T] {
	return &Decoder[T]{
		json: json.NewDecoder(r),
	}
}

// Decode reads the next JSON-encoded value from its
// input and stores it in the value pointed to by t.
//
// See the documentation for Unmarshal for details about
// the conversion of JSON into a Go value.
func (dec *Decoder[T]) Decode(t T) error {
	return dec.json.Decode(t)
}

// Buffered returns a reader of the data remaining in the Decoder's
// buffer. The reader is valid until the next call to Decode.
func (dec *Decoder[T]) Buffered() io.Reader {
	return dec.json.Buffered()
}

// Marshal proxies json.Marshal generically bound to event.Message
func Marshal[T Message](t T) ([]byte, error) {
	raw, err := json.Marshal(t)
	if err != nil {
		return raw, err
	}

	return json.Marshal(&MessageWrapper{
		N: t.Name(),
		T: t.Type(),
		M: raw,
	})
}

type MessageWrapper struct {
	N string          `json:"name"`
	T MessageType     `json:"type"`
	M json.RawMessage `json:"message"`
}

func Unmarshal(reg *MessageRegistry, data []byte) (Message, error) {
	var w MessageWrapper
	if err := json.Unmarshal(data, &w); err != nil {
		return nil, err
	}

	rt, ok := reg.Get(w.N)
	if !ok {
		return nil, errors.New("invalid message type")
	}

	var target Message = reflect.New(rt).Interface().(Message)
	if err := json.Unmarshal(w.M, &target); err != nil {
		return nil, err
	}

	return target, nil
}
