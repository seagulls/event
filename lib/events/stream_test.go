package events_test

import (
	"testing"

	qt "github.com/frankban/quicktest"

	"gitlab.com/seagulls/event/lib/events"
)

func TestMarshal(t *testing.T) {
	c := qt.New(t)

	obj := &FooEvent{}
	exp := `{"name":"foo:happened","type":"event","message":{}}`

	actual, err := events.Marshal(obj)
	c.Assert(err, qt.IsNil)
	c.Assert(string(actual), qt.Equals, exp)
}

func TestUnmarshal(t *testing.T) {
	c := qt.New(t)
	reg := events.NewMessageRegistry()
	_ = reg.Register(&FooEvent{})

	src := []byte(`{"name":"foo:happened","type":"event","message":{}}`)
	actual, err := events.Unmarshal(reg, src)
	c.Assert(err, qt.IsNil)
	c.Assert(actual, qt.ContentEquals, &FooEvent{})
}
