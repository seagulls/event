package events

import (
	"errors"
	"reflect"
)

type MessageRegistry struct {
	reg map[string]reflect.Type
}

func NewMessageRegistry() *MessageRegistry {
	return &MessageRegistry{
		reg: make(map[string]reflect.Type),
	}
}

func (r *MessageRegistry) Register(m Message) error {
	rv := reflect.ValueOf(m)

	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	rt := rv.Type()

	if mt, ok := r.reg[m.Name()]; ok {
		if mt == rt {
			return nil
		}
		return errors.New("incorrect type")
	}

	r.reg[m.Name()] = rt

	return nil
}

func (r *MessageRegistry) Get(name string) (reflect.Type, bool) {
	rt, ok := r.reg[name]

	return rt, ok
}
