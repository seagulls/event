package events_test

import (
	"reflect"
	"testing"

	qt "github.com/frankban/quicktest"

	"gitlab.com/seagulls/event/lib/events"
)

type FooEvent struct{}

func (f FooEvent) ID() string               { return `id` }
func (f FooEvent) Name() string             { return "foo:happened" }
func (f FooEvent) Type() events.MessageType { return events.TypeEvent }
func (f FooEvent) String() string           { return f.Name() }

type BarEvent struct{}

func (b BarEvent) ID() string               { return `id` }
func (b BarEvent) Name() string             { return "bar:happened" }
func (b BarEvent) Type() events.MessageType { return events.TypeEvent }
func (b BarEvent) String() string           { return b.Name() }

type FooCommand struct{}

func (f FooCommand) ID() string               { return `id` }
func (f FooCommand) Name() string             { return "do:foo" }
func (f FooCommand) Type() events.MessageType { return events.TypeCommand }
func (f FooCommand) String() string           { return f.Name() }

type BarCommand struct{}

func (b BarCommand) ID() string               { return `id` }
func (b BarCommand) Name() string             { return "do:bar" }
func (b BarCommand) Type() events.MessageType { return events.TypeCommand }
func (b BarCommand) String() string           { return b.Name() }

func TestRegistry(t *testing.T) {
	c := qt.New(t)

	reg := events.NewMessageRegistry()

	ef := &FooEvent{}
	eb := BarEvent{}
	cf := &FooCommand{}
	cb := BarCommand{}

	exEf := reflect.TypeOf(*ef)
	exEb := reflect.TypeOf(eb)
	exCf := reflect.TypeOf(*cf)
	exCb := reflect.TypeOf(cb)

	err := reg.Register(ef)
	c.Assert(err, qt.IsNil)
	err = reg.Register(eb)
	c.Assert(err, qt.IsNil)
	err = reg.Register(cf)
	c.Assert(err, qt.IsNil)
	err = reg.Register(cb)
	c.Assert(err, qt.IsNil)

	actual, ok := reg.Get("foo:happened")
	c.Assert(ok, qt.IsTrue)
	c.Assert(actual, qt.Equals, exEf)

	actual, ok = reg.Get("bar:happened")
	c.Assert(ok, qt.IsTrue)
	c.Assert(actual, qt.Equals, exEb)

	actual, ok = reg.Get("do:foo")
	c.Assert(ok, qt.IsTrue)
	c.Assert(actual, qt.Equals, exCf)

	actual, ok = reg.Get("do:bar")
	c.Assert(ok, qt.IsTrue)
	c.Assert(actual, qt.Equals, exCb)

	_, ok = reg.Get("baz:happened")
	c.Assert(ok, qt.IsFalse)
}
