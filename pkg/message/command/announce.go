package command

import (
	"fmt"

	"gitlab.com/seagulls/event/lib/events"
)

type Announce struct {
	GUID   string
	Parent string
	Team   string
}

func (a Announce) ID() string {
	return fmt.Sprintf(`%s:%s`, a.Parent, a.GUID)
}

func (Announce) Name() string {
	return `announce`
}

func (Announce) Type() events.MessageType {
	return events.TypeCommand
}

func (a Announce) String() string {
	return fmt.Sprintf("%s has scored", a.Team)
}
