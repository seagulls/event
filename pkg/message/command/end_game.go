package command

import (
	"fmt"

	"gitlab.com/seagulls/event/lib/events"
)

type EndGame struct {
	GUID   string
	Parent string
}

func (e EndGame) ID() string {
	return fmt.Sprintf(`%s:%s`, e.Parent, e.GUID)
}

func (EndGame) Name() string {
	return `endGame`
}

func (EndGame) Type() events.MessageType {
	return events.TypeCommand
}

func (EndGame) String() string {
	return `End Game`
}
