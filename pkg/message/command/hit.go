package command

import (
	"fmt"

	"gitlab.com/seagulls/event/lib/events"
)

type Hit struct {
	GUID       string
	Parent     string
	Rally      string
	From       string
	Difficulty float64
}

func (h Hit) ID() string {
	return fmt.Sprintf(`%s:%s`, h.Parent, h.GUID)
}

func (h Hit) Name() string {
	return `hit`
}

func (h Hit) Type() events.MessageType {
	return events.TypeCommand
}

func (h Hit) String() string {
	return fmt.Sprintf(`return %s's hit, difficulty %f`, h.From, h.Difficulty)
}
