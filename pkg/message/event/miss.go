package event

import (
	"fmt"
	"time"

	"gitlab.com/seagulls/event/lib/events"
)

type Miss struct {
	GUID      string
	Rally     string
	Team      string
	Player    string
	From      string
	Timestamp time.Time
}

func (m Miss) ID() string {
	return m.GUID
}

func (m Miss) Name() string {
	return `rally:miss`
}

func (m Miss) Type() events.MessageType {
	return events.TypeEvent
}

func (m Miss) String() string {
	return fmt.Sprintf(`rally:%s - %s player %s missed the ball`, m.Rally, m.Team, m.Player)
}
