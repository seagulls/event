package event

import (
	"fmt"
	"time"

	"gitlab.com/seagulls/event/lib/events"
)

type Hit struct {
	GUID       string
	Rally      string
	Team       string
	Player     string
	Difficulty float64
	Timestamp  time.Time
}

func (h Hit) ID() string {
	return h.GUID
}

func (h Hit) Name() string {
	return `rally:hit`
}

func (h Hit) Type() events.MessageType {
	return events.TypeEvent
}

func (h Hit) String() string {
	return fmt.Sprintf(`rally:%s - %s player %s hit the ball (%f)`, h.Rally, h.Team, h.Player, h.Difficulty)
}
