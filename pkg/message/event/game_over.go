package event

import (
	"fmt"
	"time"

	"gitlab.com/seagulls/event/lib/events"
)

type GameOver struct {
	GUID      string
	Timestamp time.Time
}

func (g GameOver) ID() string {
	return g.GUID
}

func (GameOver) Name() string {
	return `referee:gameOver`
}

func (GameOver) Type() events.MessageType {
	return events.TypeEvent
}

func (g GameOver) String() string {
	return fmt.Sprintf(`Game Over @ %v`, g.Timestamp)
}
