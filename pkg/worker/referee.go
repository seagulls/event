package worker

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/google/uuid"

	"gitlab.com/seagulls/event/lib/events"
	"gitlab.com/seagulls/event/pkg/message/command"
	"gitlab.com/seagulls/event/pkg/message/event"
)

type Referee struct {
	Scores *ScoreCache
	Source events.Source
	Sink   events.Sink
	hdl    *events.HandlerRegistry
	cancel context.CancelFunc
}

func NewReferee(cache *ScoreCache, in events.Source, out events.Sink, fn context.CancelFunc) *Referee {
	r := &Referee{
		Scores: cache,
		Source: in,
		Sink:   out,
		cancel: fn,
	}

	r.hdl = events.NewHandlerRegistry()
	_ = r.hdl.Register(&event.Miss{}, r.evtMiss)
	_ = r.hdl.Register(&event.GameOver{}, r.evtGameOver)
	_ = r.hdl.Register(&command.Announce{}, r.cmdAnnounce)
	_ = r.hdl.Register(&command.EndGame{}, r.cmdEndGame)

	return r
}

func (r *Referee) With(cancel context.CancelFunc) {
	r.cancel = cancel
}

func (r *Referee) Start(ctx context.Context) {
	log.Printf("worker.Referee[%p]: starting...\n", r)
	for {
		msg, err := r.Source.Get(ctx)
		if err != nil {
			return
		}

		hdl, ok := r.hdl.Get(msg)
		if !ok {
			return
		}

		if err := hdl(ctx, msg); err != nil {
			return
		}

		if err := r.Source.Ack(ctx, msg); err != nil {
			return
		}
	}
}

func (r *Referee) evtMiss(ctx context.Context, msg events.Message) error {
	m, ok := msg.(*event.Miss)
	if !ok {
		return errors.New("incorrect type")
	}

	return r.Sink.Put(ctx, &command.Announce{
		GUID:   uuid.New().String(),
		Parent: m.GUID,
		Team:   m.From,
	})
}

func (r *Referee) evtGameOver(ctx context.Context, msg events.Message) error {
	m, ok := msg.(*event.GameOver)
	if !ok {
		return errors.New("incorrect type")
	}

	return r.Sink.Put(ctx, &command.EndGame{
		GUID:   uuid.New().String(),
		Parent: m.GUID,
	})
}

func (r *Referee) cmdAnnounce(ctx context.Context, msg events.Message) error {
	m, ok := msg.(*command.Announce)
	if !ok {
		return errors.New("incorrect type")
	}

	r.Scores.Score(m.Team)

	log.Printf(
		"worker.Referee: %s has scored! Score is %s\n",
		m.Team,
		r.Scores.String(),
	)

	_, won := r.Scores.State()
	// Check whether the game is over
	if won {
		return r.Sink.Put(ctx, &event.GameOver{
			GUID:      uuid.New().String(),
			Timestamp: time.Now(),
		})
	}

	// Push a serve from the team which won the last point.
	return r.Sink.Put(ctx, &event.Hit{
		GUID:       uuid.New().String(),
		Team:       m.Team,
		Rally:      uuid.New().String(),
		Player:     `serve`,
		Difficulty: 0,
		Timestamp:  time.Now(),
	})
}

func (r *Referee) cmdEndGame(ctx context.Context, msg events.Message) error {
	if _, ok := msg.(*command.EndGame); !ok {
		return errors.New("incorrect type")
	}

	winner, _ := r.Scores.State()

	log.Printf(
		"worker.Referee: Final score is %s, %s team has won!\n",
		r.Scores.String(),
		winner,
	)

	if r.cancel == nil {
		return errors.New("The referee has no control!")
	}

	r.cancel()

	return nil
}

type RefereeFactory struct {
	Scores     *ScoreCache
	CancelFunc context.CancelFunc
}

func (p *RefereeFactory) New(ctx context.Context, ctl events.Controller) events.Worker {
	return NewReferee(p.Scores, ctl, ctl, p.CancelFunc)
}
