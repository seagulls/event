package worker

import (
	"sync"

	"github.com/davecgh/go-spew/spew"
)

type ScoreCache struct {
	Scores map[string]int
	mut    sync.RWMutex
}

func NewScoreCache() *ScoreCache {
	return &ScoreCache{
		Scores: make(map[string]int),
	}
}

func (s *ScoreCache) Score(team string) {
	s.mut.Lock()
	defer s.mut.Unlock()

	if _, ok := s.Scores[team]; !ok {
		s.Scores[team] = 0
	}

	s.Scores[team] = s.Scores[team] + 1
}

func (s *ScoreCache) State() (string, bool) {
	s.mut.RLock()
	defer s.mut.RUnlock()

	var first, second, diff int
	var team string

	for k, score := range s.Scores {
		if score >= first {
			second = first
			first = score
			team = k
		}
		diff = first - second
	}

	return team, first >= 11 && diff >= 1
}

func (s *ScoreCache) String() string {
	s.mut.RLock()
	defer s.mut.RUnlock()

	return spew.Sprint(s.Scores)
}
