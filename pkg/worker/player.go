package worker

import (
	"context"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/google/uuid"

	"gitlab.com/seagulls/event/lib/events"
	"gitlab.com/seagulls/event/pkg/message/command"
	"gitlab.com/seagulls/event/pkg/message/event"
)

type Player struct {
	ID         string
	Team       string
	Opposition string
	Skill      float64
	hdl        *events.HandlerRegistry
	Source     events.Source
	Sink       events.Sink
}

func NewPlayer(player, team, opp string, sk float64, in events.Source, out events.Sink) *Player {
	p := &Player{
		ID:         player,
		Team:       team,
		Opposition: opp,
		Skill:      sk,
		Source:     in,
		Sink:       out,
	}

	p.hdl = events.NewHandlerRegistry()
	_ = p.hdl.Register(&command.Hit{}, p.cmdHit)
	_ = p.hdl.Register(&event.Hit{}, p.evtHit)

	return p
}

func (p *Player) Start(ctx context.Context) {
	log.Printf("worker.Player[%s,%s]: starting...\n", p.Team, p.ID)
	for {
		msg, err := p.Source.Get(ctx)
		if err != nil {
			return
		}

		hdl, ok := p.hdl.Get(msg)
		if !ok {
			return
		}

		if err := hdl(ctx, msg); err != nil {
			return
		}

		if err := p.Source.Ack(ctx, msg); err != nil {
			return
		}
	}
}

func (p *Player) cmdHit(ctx context.Context, msg events.Message) error {
	m, ok := msg.(*command.Hit)
	if !ok {
		return errors.New("incorrect message type")
	}

	if m.From != p.Opposition {
		return nil
	}

	hitSkill := rand.Float64() * p.Skill
	retSkill := rand.Float64() * p.Skill

	if hitSkill >= m.Difficulty {
		return p.Sink.Put(ctx, &event.Hit{
			GUID:       uuid.New().String(),
			Rally:      m.Rally,
			Team:       p.Team,
			Player:     p.ID,
			Difficulty: retSkill,
			Timestamp:  time.Now(),
		})
	}

	return p.Sink.Put(ctx, &event.Miss{
		GUID:      uuid.New().String(),
		Rally:     m.Rally,
		Team:      p.Team,
		Player:    p.ID,
		From:      m.From,
		Timestamp: time.Now(),
	})
}

func (p *Player) evtHit(ctx context.Context, msg events.Message) error {
	m, ok := msg.(*event.Hit)
	if !ok {
		return errors.New("incorrect message type")
	}

	return p.Sink.Put(ctx, &command.Hit{
		GUID:       uuid.New().String(),
		Parent:     m.GUID,
		Rally:      m.Rally,
		From:       m.Team,
		Difficulty: m.Difficulty,
	})
}

type PlayerFactory struct {
	Team       string
	Opposition string
	ctr        int
}

func (f *PlayerFactory) New(ctx context.Context, ctl events.Controller) events.Worker {
	f.ctr++

	return NewPlayer(fmt.Sprint(f.ctr), f.Team, f.Opposition, float64(rand.Intn(8)+3), ctl, ctl)
}
