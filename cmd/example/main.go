package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/google/uuid"
	"gitlab.com/seagulls/event/lib/basic"
	"gitlab.com/seagulls/event/lib/events"
	"gitlab.com/seagulls/event/lib/mem"
	"gitlab.com/seagulls/event/pkg/message/event"
	"gitlab.com/seagulls/event/pkg/worker"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	exchange := mem.NewExchange()

	// ready referee
	refReg := events.NewMessageRegistry()
	if err := refReg.Register(&event.Miss{}); err != nil {
		log.Fatal(err)
	}
	if err := refReg.Register(&event.GameOver{}); err != nil {
		log.Fatal(err)
	}

	refQ := mem.NewQueue()
	refExAdapter := exchange.Register(`ref`)
	refEx := basic.NewExchange(refReg, refExAdapter, refExAdapter, refQ)
	refCtl := basic.NewController(refEx, refQ, &worker.RefereeFactory{
		Scores:     worker.NewScoreCache(),
		CancelFunc: cancel,
	})

	// ready team Ping
	pingReg := events.NewMessageRegistry()
	if err := pingReg.Register(&event.Hit{}); err != nil {
		log.Fatal(err)
	}

	pingQ := mem.NewQueue()
	pingExAdapter := exchange.Register(`ping`)
	pingEx := basic.NewExchange(pingReg, pingExAdapter, pingExAdapter, pingQ)
	pingCtl := basic.NewController(pingEx, pingQ, &worker.PlayerFactory{
		Team:       `ping`,
		Opposition: `pong`,
	})

	// ready team Pong
	pongReg := events.NewMessageRegistry()
	if err := pongReg.Register(&event.Hit{}); err != nil {
		log.Fatal(err)
	}

	pongQ := mem.NewQueue()
	pongExAdapter := exchange.Register(`pong`)
	pongEx := basic.NewExchange(pongReg, pongExAdapter, pongExAdapter, pongQ)
	pongCtl := basic.NewController(pongEx, pongQ, &worker.PlayerFactory{
		Team:       `pong`,
		Opposition: `ping`,
	})

	// start both teams
	go refCtl.Start(ctx)
	go pingCtl.Start(ctx)
	go pongCtl.Start(ctx)

	<-time.After(time.Second)

	if err := exchange.Put(ctx, &event.Hit{
		GUID:       uuid.New().String(),
		Rally:      uuid.New().String(),
		Team:       "ping",
		Player:     "serve",
		Difficulty: 0,
		Timestamp:  time.Now(),
	}); err != nil {
		log.Fatal(err)
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	<-sigChan

	// stop all the nonsense
	cancel()
}
