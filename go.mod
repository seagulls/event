module gitlab.com/seagulls/event

go 1.19

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/frankban/quicktest v1.14.4
	github.com/google/uuid v1.3.0
)

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
)
