# Event Lib

This library and supporting code is a POC example of implementing event driven services in Go.

## The Library

The Event lib consists of three packages;

- `lib/events` contains the structures and interfaces modelling `Messages` (events and commands), `Workers` and definitions for the service adapters.  These are described [below](#interfaces).
- `lib/mem` contain two simple, in-memory examples of a `Queue` and `Exchange` which may be used in place of spinning up supporting queue/broker services.  It is intended that other sample implementations of these interfaces be created for SQS/SNS/etc...
- `lib/basic` contain example implementations of the `Controller`, `Exchange` and `Queue` service components.

## Interfaces

- `Message`: represents a message as sent through the event broker and internal service queues.  The interface is simple and only defines two methods which return the message name and type.  The message name should be a hierarchically structured string, such as a sequence of domain > subdomain names ending with the event or command name.
- `Command`: is a `Message` which can only represent a command type message.
- `Event`: is a `Message` which can only represent an event type message.
- `Worker`: another simple interface allowing a worker to be started.  The only parameter to the `Start()` method is a context which should be used to determine when to exit the main loop.  Any other setup for the worker should be carried out and injected before the worker is created.  The `Worker` should regularly fetch `Messages` from the `Controller` and should put response `Event` or `Command` messsages back to the `Controller`.
- `WorkerFactory`: creates new `Worker`s.
- `Service`: a `Service` has the same interface as a `Worker` but with additional methods to initialise and shutdown.
- `Source`: This interface provides two methods to obtain `Messages`. `Get` will obtain a single message from the `Source`, while `Ack` will acknowledge the message as having been processed.  In both these cases the specifics of how the message is fetched and where it is fetched from are left up to the implementation of the relevant service adapter.
- `Sink`: This is the opposite of `Source` and allows for a `Message` to be published through the `Sink`.
- `Exchange`: the interface to the central event exchange.  `Exchange` implementations will typically have at least one internal `Sink` and an external `Source` and `Sink`. `Exchanges` deal exclusively with Event type messages and should ack messages from the external source on successfully passing them through to the internal Sink via internal Exchange workers.
- `Queue`: the adapter for the microservice's internal message queue, this will hold all message types.
- `Controller`: the interface for the microservice domain controller and acts as the controller for the `Exchange` and the `Queue`. Domain `Workers` are created by the `Controller`, directing `Event` messages to the `Exchange.Sink` and Command messages to the `Queue.Sink`.

## Example

There is a basic example included in `pkg`.  This basic example creates three pseudo-services (they're just separate goroutines in reality) and makes use of the in-memory mock exchange and queue.  The system allows for two teams to play a game of `ping-pong` (or table tennis, your choice) over an event exchange.  Currently this is set up with two players on each team and a pair of referees keeping score.  The game will finish when one team has passed 11 points and is at least 2 points ahead of the other team.

### Example Events

- `rally:hit` this is emitted when one of the teams successfully hits the ball to the other team.  Team `ping` will pick up `pong` events and vice-versa.  Hit events contain a floating point value indicating the difficulty of returning the shot.  Hit events will result in a `hit` command.
- `rally:miss` is emitted when one of the teams fails to hit the ball back to the opposing team and is picked up by the Referee.  Miss events result in an `announce` command.
- `referee:game-over` is emitted when a team scores and the game winning conditions have been met.

### Example Commands

- `hit` is emitted when an opposing team hit event is received.  Processing the command involves the Worker (player) checking a random value to see if they exceed the incoming shot difficulty.  If they are successful then they emit their team's hit event with a random difficulty based on the player's skill.  If they fail then they emit their team's miss event.
- `announce` is emitted by the referee when they see a miss event.  Processing the command increments the teams score and a check of the win conditions.  If the game has been won, a game-over event is emitted, otherwise a `hit` event of difficulty 0 (aka a serve) is emitted as if from the team which won the point.
- `end-game` is emitted when a game-over event is seen by the referee. This announces the winner and final score and terminates the game.
